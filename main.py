import argparse
import logging
import numpy as np
import sys
import json
import cplex
from pathlib import Path
from os.path import join

from benders.args import config_group, process_args
from benders.master import Benders
from benders.report import Report
from benders.logger import Log
from benders.utils import isZero, toKey, toSol
from benders.timer import Timer

from load import edges, make_aux_vars, make_master_vars, make_sub_vars
from load import load_master_constraints, read_files, read_index_demension
from load import extend_bd

from complete import Complete
from waste import WS, Round_WS

# CLI
parser = argparse.ArgumentParser(description='Locating-Routing',
                                 formatter_class=argparse.RawTextHelpFormatter)

config_group(parser)

waste = parser.add_argument_group("waste.py")

waste.add_argument("-I", type=int, default=11)
waste.add_argument("-R", type=int, default=7)
waste.add_argument("-T", type=int, default=4)
waste.add_argument("-L", type=int, default=2)
waste.add_argument("-U", type=int, default=4)
waste.add_argument("-Q", type=int, default=3)
waste.add_argument("-TL", type=int, default=180)
waste.add_argument("--alpha", type=int, default=10)
waste.add_argument("--folder",
                   type=str,
                   default="n5t4_1",
                   help="Folder where the instance files are located.")
waste.add_argument("--mip",
                   action="store_true",
                   help="Solve the problem using a complete MIP formulation")
waste.add_argument("--write-sol",
                   action="store_true",
                   help="Write out the solution in "
                   "Solutions/<instance>/(results|mip).txt")
waste.add_argument("--write-iterations",
                   action="store_true",
                   help="Write out all open solutions in "
                   "Solutions/<instance>/(results|mip)-nn.txt")
waste.add_argument("--auto-heur",
                   action="store_true",
                   help="Use CPLEX for heuristic")
waste.add_argument("--tune",
                   type=int,
                   default=0,
                   help="Use CPLEX tuning, this will set the MIP emphasis.")
waste.add_argument("--order",
                   action="store_true",
                   help="Branch on GAP variables first")
waste.add_argument("--vis",
                   action="store_true",
                   help="Use Valid Inequalities (VIs)")
waste.add_argument("--exact",
                   action="store_true",
                   help="Indicate when the heuristic is exact.")
waste.add_argument("--extend",
                   action="store_true",
                   help="Use partial Benders, aka extended master problem.")
waste.add_argument("--l-shaped",
                   action="store_true",
                   help="Use L-shaped cuts.")


def find_min_rhs(ms, b_i, beta_r):
    cpx = cplex.Cplex()
    cpx.set_warning_stream(None)
    cpx.set_results_stream(None)
    cpx.set_log_stream(None)

    cpx.variables.add(
        [b_i[i] * beta_r[r] for i, r in ms],
        types="B" * len(ms),
        names=list(ms.values()))

    for i, b in enumerate(b_i):
        if i == 0:
            continue

        cpx.linear_constraints.add(
            [([ms[i, r] for r in range(len(beta_r))], [1] * len(beta_r))],
            "E",
            [1])

    cpx.solve()

    return cpx.solution.get_values()


def write_solution(name, instance, R, T, L, I, xs, ms, vs, ns, all_variables,
                   objective_coef, alloc_costs):
    with open(join("Solutions", instance, name + ".txt"), 'w') as f:
        total_length = len(list(ms.values())) + len(list(xs.values())) + len(
            list(vs.values()))

        print(
            "The GAP with the furthest distance from the depot has been "
            "reassigned to the last position of set I \n",
            file=f)

        for m in ms.values():
            if not isZero(all_variables[m]):
                print("{}\t{:d}".format(m, int(all_variables[m])), file=f)

        count = 0.0
        for t in range(T):
            for l in range(L):
                first = True
                for i, i2 in edges(I):
                    x = "x.i{}.i{}.l{}.t{}".format(i, i2, l, t)
                    if not isZero(all_variables[x]):
                        if first:
                            print("\nDay {}, Vehicle {}\n".format(t, l),
                                  file=f)
                            first = False

                        count += objective_coef[i, i2, l, t] * int(
                            all_variables[x])
                        print("{}\t{:d}".format(x, int(all_variables[x])),
                              file=f)

        print("\nTotal transport cost " + str(count) + "\n", file=f)

        for t in range(T):
            for l in range(L):
                first = True
                for i, i2 in edges(I):
                    v = "v.i{}.i{}.l{}.t{}".format(i, i2, l, t)
                    if not isZero(all_variables[v]):
                        if first:
                            print("\nDay {}, Vehicle {}\n".format(t, l),
                                  file=f)
                            first = False

                        print("{}\t{:f}".format(v, all_variables[v]), file=f)

        print("\nGAPs\n", file=f)

        allocation = 0
        for (i, u), n in ns.items():
            if not isZero(all_variables[n]):
                allocation += alloc_costs[u]
                print("{}\t{:d}".format(n, int(all_variables[n])), file=f)

        print("\nTotal bin allocation cost {}".format(allocation), file=f)


def solve_benders(bd_wa, instance, I, T, L, U, R, a_rt, b_i, beta_r, cin_u,
                  cap_u, d_ij, s_i, alfa, TL, Q, xs, ms, vs, objective_coef,
                  auto_heur, verbose, use_vis, tlim, order, exact, extend,
                  l_shaped):
    bd_wa.setMinimise()

    bd_wa.addVars(objs=np.zeros((I - 1) * R), names=list(ms.values()))  # 6
    bd_wa.addVars(objs=list(objective_coef.values()),
                  names=list(xs.values()),
                  auxiliary=True)  # 108
    bd_wa.addVars(objs=np.zeros(I * I * L * T),
                  lbs=[0] * I * I * L * T,
                  names=list(vs.values()),
                  extended=True,
                  vtype="C")  # 108

    expr, names, rhs, senses = load_master_constraints(I, R, T, L, Q, TL, xs,
                                                       ms, vs, a_rt, s_i, d_ij,
                                                       b_i, beta_r, use_vis)
    bd_wa.addConstrs(expr, senses, rhs, names)

    # Subproblem addition
    int_sub = WS(I, U, R, cin_u, cap_u, beta_r, b_i, instance, True, verbose)
    bd_wa.addSub({0: WS(I, U, R, cin_u, cap_u, beta_r, b_i, instance,
                        verbose=verbose)})

    if l_shaped:
        sol = find_min_rhs(ms, b_i, beta_r)
        l_value = 0
        for sub_id, sub in bd_wa.sub_pbs.items():
            sub.solve(sol)
            l_value += sub.value()

        bd_wa.addLShaped(l_value)

    if not auto_heur:
        bd_wa.addHeur(
            {0: Round_WS(I, U, R, cin_u, cap_u, beta_r, b_i, instance)})

        if exact:
            bd_wa.isExactHeur()
    else:
        auto = WS(I, U, R, cin_u, cap_u, beta_r, b_i, instance, True, verbose)
        auto.cpx.parameters.mip.limits.solutions.set(1)
        bd_wa.addHeur({0: auto})

    bd_wa.addIntSub({0: int_sub})

    # Parameters setting for solving
    # bd_wa.setTimeLimit(600)
    bd_wa.setMaxIters(10000000)

    if order:
        # 0 is default branching order
        bd_wa.master.order.set((m, 10, 0) for m in ms.values())

    if extend:
        extend_bd(bd_wa, I, U, cin_u, cap_u, b_i, beta_r)

    # Write down the log
    if verbose > 0:
        # Write down the master
        bd_wa.master.write(join("Solutions", instance, "waste.lp"))

        if verbose > 1:         # Output CPLEX log to stdout
            bd_wa.setCPLEXOutput(sys.stdout)
        else:
            bd_wa.setCPLEXOutput(
                filename=join("Solutions", instance, "cplex.log"))

    if tlim is not None:
        bd_wa.setTimeLimit(tlim)

    # Solving
    bd_wa.solve()

    return int_sub


def solve_as_mip(I, T, L, U, R, a_rt, b_i, beta_r, cin_u, cap_u, d_ij, s_i,
                 alfa, TL, Q, ns, xs, ms, vs, cins, cis, instance, verbose,
                 use_vis, tlim):
    """
    Build a complete CPLEX model to verify Benders's results.
    """
    mip = Complete()
    mip.make_vars(ns, xs, ms, vs, cins, cis, cin_u, d_ij, alfa)
    mip.make_cstrs(I, T, L, U, R, Q, TL, a_rt, s_i, d_ij, b_i, beta_r, cap_u,
                   use_vis)

    if verbose:
        mip.cpx.set_log_stream(sys.stdout)
        mip.cpx.set_results_stream(sys.stdout)
        mip.cpx.set_warning_stream(sys.stdout)

        mip.write(join("Solutions", instance, "mip"))

    if tlim is not None:
        mip.cpx.parameters.timelimit.set(tlim)

    with Timer() as t:
        mip.solve()

    Log.info("Solved MIP, objective = {}".format(mip.value()))

    Report.solve_time = t.interval
    Report.solution = mip.cpx.solution.get_objective_value()

    return mip


def main():
    logging.basicConfig()
    Log.setLevel(logging.INFO)
    bd_wa = Benders()
    args = parser.parse_args()

    process_args(args, bd_wa)
    bd_wa.enableSaveVars()  # Force variable saving

    # I = J = 88; R = 6; T = 7; L = 10;
    I = args.I  # = J
    R = args.R
    T = args.T
    L = args.L
    U = args.U
    Q = args.Q
    TL = args.TL
    instance = args.folder
    # alfa = args.alpha

    # complete_folder = "Instances/" + instance
    complete_folder = instance
    Path("Solutions/" + instance).mkdir(parents=True, exist_ok=True)

    try:
        I, T, L, U, R = read_index_demension(complete_folder)
    except:
        print("No index dimension file, using argument command line or "
              "default values")

    a_rt, b_i, beta_r, cin_u, cap_u, d_ij, s_i, alfa, TL, Q = read_files(
        complete_folder)
    objective_coef, xs = make_master_vars(I, L, T, d_ij, s_i, alfa)
    ms, vs = make_aux_vars(I, R, L, T)
    ns, cins = make_sub_vars(I, U, cin_u)

    if args.mip:
        mip = solve_as_mip(I, T, L, U, R, a_rt, b_i, beta_r, cin_u, cap_u,
                           d_ij, s_i, alfa, TL, Q, ns, xs, ms, vs, cins,
                           objective_coef, instance, args.verbose > 0, args.vis,
                           args.time)
        all_variables = dict(
            zip(mip.cpx.variables.get_names(), mip.cpx.solution.get_values()))
        solname = "mip"
    else:
        if args.tune:
            # Tune according to CPLEX recommendations
            # https://www.ibm.com/support/pages/cplex-performance-tuning-mixed-integer-programs#Item3
            #
            # Setting emphasis=3 seems *real* slow, and we do want to explore
            # some solutions to generate cuts.
            bd_wa.master.parameters.emphasis.mip.set(args.tune)
            bd_wa.master.parameters.emphasis.memory.set(True)
            bd_wa.master.parameters.mip.strategy.probe.set(3)

        sub = solve_benders(bd_wa, instance, I, T, L, U, R, a_rt, b_i, beta_r,
                            cin_u, cap_u, d_ij, s_i, alfa, TL, Q, xs, ms, vs,
                            objective_coef, args.auto_heur, args.verbose,
                            args.vis, args.time, args.order, args.exact,
                            args.extend, args.l_shaped)
        final = bd_wa.postprocessing()

        if args.exact:
            # Have to *generate* the subproblem's solution
            sub.solve(final[:len(ms)])

        var_names = bd_wa.master_vars + bd_wa.aux_vars + bd_wa.ext_vars
        all_variables = dict(zip(var_names, bd_wa.values[toKey(final)]))
        # Filter out auxiliary variables
        all_variables.update(sub.solutions[toKey(final[:len(ms)])])
        solname = "results"

    if args.no_heur:
        solname += "-noheur"

    if args.auto_heur:
        solname += "-auto"

    if args.vis:
        solname += "-vis"

    if args.tune:
        solname += "-tune{}".format(args.tune)

    if args.order:
        solname += "-order"

    if args.extend:
        solname += "-extend"

    if args.subopt:
        solname += "-sub{}".format(args.subopt)

    if args.l_shaped:
        solname += "-L"

    Report.json(join("Solutions", instance, solname + ".json"))

    if args.write_sol:
        write_solution(solname, instance, R, T, L, I, xs, ms, vs, ns,
                       all_variables, objective_coef, cin_u)

    if args.write_iterations:
        assert not args.mip, "Cannot write out iterations with MIP"

        # Iterate over (sorted) open solutions that have been solved
        for i, key in enumerate(bd_wa.solved):
            # All variables values
            all_variables = dict(zip(var_names, bd_wa.values[key]))
            # We need to de-hash the solution, take the right number of
            # variables, then re-hash the resulting list .-.
            sub_key = toKey(toSol(key)[:len(ms)])
            # Filter out auxiliary variables
            all_variables.update(sub.solutions[sub_key])
            write_solution("{}-{:d}".format(solname, i), instance, R, T, L, I,
                           xs, ms, vs, ns, all_variables, objective_coef, cin_u)


if __name__ == "__main__":
    #print(main())
    main()
