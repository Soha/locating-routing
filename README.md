# Locating Routing

## Diego Rossit

A problem that appears in the context of MSW is the problem of
establishing the best locations of garbage accumulation
points (GAP) in an urban area. A GAP is a place where some waste
bins (hereafter bins) are installed. The citizens can carry their
waste to these places and the accumulated garbage will be
periodically collected by the collection vehicles. The GAPs are
usually located according to different objectives, e.g.,
minimizing the (average) distance to the users, minimizing the
total investment cost in purchasing and/or installing bins,
etc. Generally, the GAPs can not be located anywhere. There are
predefined locations in an urban area where a GAP can be opened,
i.e., install bins. Trough an optimization process the best
location and the other locations remain unused.

## Requirements

You will need to install [BranDec](https://gitlab.com/Soha/brandec) in order to
run the code. Main dependencies will include CPLEX v12.5 and numpy.

## Usage

The easiest way to get started is to use one of the provided instances. Custom
instances can be generated using the appropriate parameters (in capitals).

``` sh
python main.py --folder data/<instance>
```

Common options are:

``` 
  --mip                 Solve the problem using a complete MIP formulation
  --vis                 Use Valid Inequalities (VIs)
  --time TIME           Time limit in seconds.
```

Results will be output to: `Solutions/<instance>/(results|mip).json`

## Disclaimer

This code is made available for the benefit of the scientific community and
comes with no guarantee.
