#
# This file takes care of the "loading" functions, that is converting the
# instance files into actionable variables for BranDec and CPLEX -- especially
# in the `add_*` functions for the latter.
#
import logging
import numpy as np

from collections import OrderedDict
from os.path import join
from itertools import permutations

from benders.logger import Log


def edges(I, start=0):
    return permutations(range(start, I), 2)


def add_max_capacity(expr, names, rhs, senses, I, U, cap_u, b_i=None,
                     beta_r=None, with_ms=False):
    """
    Create constraints (1a)
    """
    for i in range(1, I):
        ind = []
        val = []
        for u in range(U):
            ind.append('n.i{}.u{}'.format(i, u))
            val.append(cap_u[u].tolist())

        # Used for Complete
        if with_ms:
            for r, beta in enumerate(beta_r):
                ind.append("m.i{}.r{}".format(i, r))
                val.append((-1) * b_i[i] * beta)

        names.append("(1a).i{}".format(i))
        expr.append([ind, val])
        rhs.append(0)
        senses.append("G")


def add_bin_arrangement(expr, names, rhs, senses, I, U):
    """
    Create constraints (1b)
    """
    for i in range(1, I):
        ind = []
        for u in range(U):
            ind.append('n.i{}.u{}'.format(i, u))

        expr.append([ind, [1] * U])
        names.append("(1b).i{}".format(i))
        senses.append("E")
        rhs.append(1)


def add_single_visit(expr, names, rhs, senses, I, R, ms):
    """
    Create constraints (1c)
    """
    for i in range(1, I):
        ind = [ms[i, r] for r in range(R)]
        val = np.ones(R)

        expr.append([ind, val])
        senses.append("E")
        rhs.append(1)
        names.append('(1c).i{}'.format(i))


def add_79_b(expr, names, rhs, senses, I, R, T, L, xs):
    """
    Create constraints (78)
    """
    # TODO deprecated?
    ind = [xs[0, 1, 0, 0]]
    val = [1]
    expr.append([ind, val])
    senses.append("E")
    rhs.append(1)
    names.append("(79_b)")


def add_veh_visit(expr, names, rhs, senses, I, R, T, L, ms, xs, a_rt):
    """
    Create constraints (1e)
    """
    for i in range(1, I):
        for t in range(T):
            exp_list = []
            val = []
            for i2 in range(0, I):
                if i2 != i:
                    for l in range(L):
                        exp_list.append(xs[i, i2, l, t])
                        val.append(1)

            exp_list_2 = []
            val_2 = []
            for r in range(R):
                exp_list_2.append(ms[i, r])
                val_2.append((-1) * a_rt[r][t].tolist())

            expr.append([exp_list + exp_list_2, val + val_2])
            senses.append("E")
            rhs.append(0)
            names.append("(1e).i{}.t{}".format(i, t))


def add_day_visit(expr, names, rhs, senses, I, T, L, xs):
    """
    Create constraints (1d)
    """
    for i in range(0, I):
        for t in range(T):
            for l in range(L):
                exp_list = []
                val = []
                exp_list_2 = []
                val_2 = []
                for i2 in range(I):
                    if i2 != i:
                        exp_list.append(xs[i2, i, l, t])
                        val.append(1)
                        exp_list_2.append(xs[i, i2, l, t])
                        val_2.append(-1)

                expr.append([exp_list + exp_list_2, val + val_2])
                senses.append("E")
                rhs.append(0)
                names.append("(1d).i{}.l{}.t{}".format(i, l, t))


def add_veh_use(expr, names, rhs, senses, I, T, L, xs):
    """
    Create constraints (1f)
    """
    for t in range(T):
        for l in range(L):
            exp_list = []
            val = []
            for i in range(1, I):
                exp_list.append(xs[0, i, l, t])
                val.append(1)

            expr.append([exp_list, val])
            senses.append("L")
            rhs.append(1)
            names.append("(1f).l{}.t{}".format(l, t))


def add_time_limit(expr, names, rhs, senses, I, T, L, TL, xs, s_i, d_ij):
    """
    Create constraints (1g)
    """
    for t in range(T):
        for l in range(L):
            exp_list = []
            val = []
            for i, i2 in edges(I):
                exp_list.append(xs[i, i2, l, t])
                val.append((s_i[i] + d_ij[i, i2]).tolist())

            expr.append([exp_list, val])
            senses.append("L")
            rhs.append(TL)
            names.append("(1g).l{}.t{}".format(l, t))


def add_waste_limit(expr, names, rhs, senses, I, T, L, Q, xs, vs):
    """
    Create constraints (1h)
    """
    for t in range(T):
        for l in range(L):
            for i, i2 in edges(I):
                ind = [vs[i, i2, l, t], xs[i, i2, l, t]]
                val = [1, -Q]

                expr.append([ind, val])
                senses.append("L")
                rhs.append(0)
                names.append("(1h).i{}.g{}.l{}.t{}".format(i, i2, l, t))


def add_balance(expr, names, rhs, senses, I, R, T, L, Q, ms, xs, vs, b_i,
                beta_r):
    """
    Create constraints (1i)
    """
    for t in range(T):
        for l in range(L):
            for i2 in range(1, I):
                exp_list_v1 = []
                val_v1 = []
                exp_list_v2 = []
                val_v2 = []
                exp_list_x = []
                val_x = []
                for i in range(0, I):
                    if i2 != i:
                        exp_list_v1.append(vs[i, i2, l, t])
                        val_v1.append(1)
                        exp_list_v2.append(vs[i2, i, l, t])
                        val_v2.append(-1)
                        exp_list_x.append(xs[i, i2, l, t])
                        val_x.append(Q)

                exp_list_m = []
                val_m = []
                for r in range(R):
                    exp_list_m.append(ms[i2, r])
                    val_m.append((b_i[i2] * beta_r[r]).tolist())

                expr.append([
                    exp_list_v1 + exp_list_v2 + exp_list_x + exp_list_m,
                    val_v1 + val_v2 + val_x + val_m
                ])
                senses.append("L")
                rhs.append(Q)
                names.append("(1i).i{}.l{}.t{}".format(i2, l, t))


def add_empty_start(expr, names, rhs, senses, I, R, T, L, ms, vs, b_i, beta_r):
    """
    Create constraints (1j)
    """
    for t in range(T):
        for l in range(L):
            for i2 in range(1, I):
                expr.append([[vs[0, i2, l, t]], [1]])
                senses.append("E")
                rhs.append(0)
                names.append("(1j).i{}.l{}.t{}".format(i2, l, t))


def vi_depot_only(expr, names, rhs, senses, I, T, L, xs):
    """
    Create constraints (86) - valid inequality

    Meaning: if a vehicle does not depart from the depot, it cannot perform any
    trip between any two clients on that day.
    """
    for t in range(T):
        for l in range(L):
            exp_list_v1 = []
            val_v1 = []
            for i in range(1, I):
                for i2 in range(0, I):
                    if i2 != i:
                        exp_list_v1.append(xs[i, i2, l, t])
                        val_v1.append(1)
            exp_list_v2 = []
            val_v2 = []
            for i in range(1, I):
                # TODO I changed an `i3` here, correct
                exp_list_v2.append(xs[0, i, l, t])
                val_v2.append((-1) * I * I)
            expr.append([exp_list_v1 + exp_list_v2, val_v1 + val_v2])
            senses.append("L")
            rhs.append(0)
            names.append("(86).i{}.i{}.l{}.t{}".format(i, i2, l, t))


def add_vi_veh_unused(expr, names, rhs, senses, I, T, L, xs):
    """
    Create constraints (88) - valid inequality

    Meaning: if during a day vehicle l remain unused, the subsequent vehicle (l
    + 1) also remains unused.

    TODO This is not in the model? Now yes
    """
    for t in range(T):
        for l in range(1, L):
            exp_list_v1 = []
            exp_list_v2 = []
            val_v1 = []
            val_v2 = []
            for i in range(1, I):
                exp_list_v1.append(xs[0, i, l - 1, t])
                val_v1.append(-1)
                exp_list_v2.append(xs[0, i, l, t])
                val_v2.append(1)

            expr.append([exp_list_v1 + exp_list_v2, val_v1 + val_v2])
            senses.append("L")
            rhs.append(0)
            names.append("(88).l{}.t{}".format(l, t))


def add_vi_single_direction(expr, names, rhs, senses, I, T, L, xs):
    """
    Create constraints (89) - valid inequality

    Meaning: a bidirectional arc between any two clients is traversed only in
    one sense on each day.

    TODO This is not in the model? Now yes
    """
    for t in range(T):
        for l in range(L):
            for i, i2 in edges(I, start=1):
                expr.append(([xs[i, i2, l, t], xs[i2, i, l, t]], [1, 1]))
                senses.append("L")
                rhs.append(1)
                names.append("(89).i{}.i{}.l{}.t{}".format(i, i2, l, t))


def add_furthest_GAP(expr, names, rhs, senses, I, T, L, xs):
    """
    Create constraints () - valid inequality

    Meaning: tray to imitate the functioning of the constraint in
    http://arxiv.org/abs/1612.01691 with the furthest node (4.5.4 Customer
    Assignment) However, since we don't know a priori which would be the
    furthest GAP scehduled for each day (we depend on the product m_ir * a_rt)
    we scheduled the node with the furthest index

    TODO This is not in the model? Now yes
    """

    exp_list_v1 = []
    val_v1 = []
    for t in range(T):
        for i in range(I - 1):
            #exp_list_v1.append(xs[i, I - 1, 0, t])
            #val_v1.append(1)
            for l in range(1,L):
                exp_list_v1.append(xs[i, I - 1, l, t])
                val_v1.append(1)

    expr.append([exp_list_v1, val_v1])
    #senses.append("G")
    #rhs.append(1)
    senses.append("E")
    rhs.append(0)
    names.append("(furthest_GAP)")


def vi_depot_stay(expr, names, rhs, senses, I, T, L, xs):
    """
    Create constraints (90) - valid inequality

    Meaning: if a vehicle is not going to serve any GAP, it does not leave the
    depot that day (inverse meaning of valid inequality 86).
    """
    for t in range(T):
        for l in range(L):
            exp_list_v1 = []
            exp_list_v2 = []
            val_v1 = []
            val_v2 = []
            for i in range(1, I):
                exp_list_v1.append(xs[0, i, l, t])
                val_v1.append(1)
                for i2 in range(1, I):
                    if i2 != i:
                        exp_list_v2.append(xs[i, i2, l, t])
                        val_v2.append(-1)

            expr.append([exp_list_v1 + exp_list_v2, val_v1 + val_v2])
            senses.append("L")
            rhs.append(0)
            names.append("(90).l{}.t{}".format(l, t))


def vi_min_trips(expr, names, rhs, senses, I, T, L, R, xs, ms, b_i, a_rt,
                 beta_r, Q):
    """
    Create constraints (91) - valid inequality

    Meaning: minimum number of trips required on each day. Only if there is an
    homogeneous fleet.
    """
    # total_waste = 0.0
    for t in range(T):
        exp_list_v1 = []
        val_v1 = []
        exp_list_v2 = []
        val_v2 = []
        for i in range(1, I):
            for l in range(L):
                exp_list_v1.append(xs[0, i, l, t])
                val_v1.append(1)
            for r in range(R):
                exp_list_v2.append(ms[i, r])
                val_v2.append(((-1) * b_i[i] * a_rt[r, t] * beta_r[r]) / Q)
        expr.append([exp_list_v1 + exp_list_v2, val_v1 + val_v2])
        senses.append("G")
        rhs.append(0)
        names.append("(91).t{}".format(t))


def read_array(folder, fname, loadtype):
    return np.array(
        np.loadtxt(join(folder, fname), dtype=loadtype, delimiter='\t'))


def reorder_furthest_GAP(d_ij, b_i, s_i):

    max_ = d_ij[0, 1]
    furthest = 1
    for j in range(2, d_ij.shape[1] - 1):
        if (d_ij[0, j] > max_):
            max_ = d_ij[0, j]
            furthest = j

    d_ij[:, [furthest, d_ij.shape[0] - 1]] = \
        d_ij[:, [d_ij.shape[0] - 1, furthest]]
    d_ij[[furthest, d_ij.shape[0] - 1], :] = \
        d_ij[[d_ij.shape[0] - 1, furthest], :]
    b_i[furthest], b_i[d_ij.shape[0] - 1] = \
        b_i[d_ij.shape[0] - 1], b_i[furthest]
    s_i[furthest], s_i[d_ij.shape[0] - 1] = \
        s_i[d_ij.shape[0] - 1], s_i[furthest]

    return d_ij, b_i, s_i


def read_files(folder):
    # reading files
    a_rt = read_array(folder, "a_rt.txt", np.int)
    b_i = read_array(folder, "b_i.txt", np.float)
    beta_r = read_array(folder, "beta_r.txt", np.int)
    cin_cap_u = read_array(folder, "cin_u_cap_u.txt", np.float)
    cin_u = cin_cap_u[:, 0]
    cap_u = cin_cap_u[:, 1]
    d_ij = read_array(folder, "d_ij.txt", np.float)
    # es_i = np.array(np.loadtxt("es_i.txt", dtype=np.int, delimiter='\t'))
    s_i = read_array(folder, "s_i.txt", np.float)
    d_ij, b_i, s_i = reorder_furthest_GAP(d_ij, b_i, s_i)
    Others = [None] * 3
    i = 0
    with open(folder + "/Other_param.txt") as fp:
        line = fp.readline().strip()
        while line:
            Others[i] = float(line.split('\t')[1])
            i = i + 1
            line = fp.readline()

    return (a_rt, b_i, beta_r, cin_u, cap_u, d_ij, s_i, Others[0], Others[1],
            Others[2])


def read_index_demension(folder):
    # reading files
    with open(folder + "/Sets_size.txt") as fp:
        line = fp.readline()
        I = int(line.split('\t')[1])
        line = fp.readline()
        T = int(line.split('\t')[1])
        line = fp.readline()
        L = int(line.split('\t')[1])
        line = fp.readline()
        U = int(line.split('\t')[1])
        line = fp.readline()
        R = int(line.split('\t')[1])

    return I, T, L, U, R


def make_sub_vars(I, U, cin):
    ns = OrderedDict()
    coefs = []

    for u in range(U):
        coefs.extend([float(cin[u])] * (I - 1))

        for i in range(1, I):
            ns[(i, u)] = "n.i{}.u{}".format(i, u)

    return ns, coefs


def make_aux_vars(I, R, L, T):
    #
    ms = OrderedDict([((i, r), "m.i{}.r{}".format(i, r)) for i in range(1, I)
                      for r in range(R)])

    vs = OrderedDict([((i, i2, l, t), 'v.i{}.i{}.l{}.t{}'.format(i, i2, l, t))
                      for i in range(I) for i2 in range(I) for l in range(L)
                      for t in range(T)])

    return ms, vs


def make_master_vars(I, L, T, d_ij, s_i, alfa):
    objective_coef = OrderedDict(
        [((i, i2, l, t), float(d_ij[i, i2]) + float(s_i[i]) * alfa)
         for i, i2 in edges(I) for l in range(L)
         for t in range(T)])

    xs = OrderedDict([((i, i2, l, t), 'x.i{}.i{}.l{}.t{}'.format(i, i2, l, t))
                      for i, i2 in edges(I) for l in range(L)
                      for t in range(T)])

    return objective_coef, xs


def load_master_constraints(I, R, T, L, Q, TL, xs, ms, vs, a_rt, s_i, d_ij,
                            b_i, beta_r, use_vis):
    # Master constraints
    expr = []
    names = []
    rhs = []
    senses = []

    add_single_visit(expr, names, rhs, senses, I, R, ms)
    # add_79_b(expr, names, rhs, senses, I, R, T, L, xs)
    add_veh_visit(expr, names, rhs, senses, I, R, T, L, ms, xs, a_rt)
    add_day_visit(expr, names, rhs, senses, I, T, L, xs)
    add_veh_use(expr, names, rhs, senses, I, T, L, xs)
    add_time_limit(expr, names, rhs, senses, I, T, L, TL, xs, s_i, d_ij)
    add_waste_limit(expr, names, rhs, senses, I, T, L, Q, xs, vs)
    add_balance(expr, names, rhs, senses, I, R, T, L, Q, ms, xs, vs, b_i,
                beta_r)
    add_empty_start(expr, names, rhs, senses, I, R, T, L, ms, vs, b_i, beta_r)

    if use_vis:
        add_vi_veh_unused(expr, names, rhs, senses, I, T, L, xs)
        add_vi_single_direction(expr, names, rhs, senses, I, T, L, xs)
        add_furthest_GAP(expr, names, rhs, senses, I, T, L, xs)

    return expr, names, rhs, senses


def extend_bd(bd, I, U, cin_u, cap_u, b_i, beta_r):
    expr = []
    names = []
    rhs = []
    senses = []
    # Add LP relaxation of subproblem to master
    ns, coefs = make_sub_vars(I, U, cin_u)
    bd.addExtVars(lbs=[0] * (I - 1) * U,
                  names=list(ns.values()),
                  vtype="C")

    # Add the (underestimate) of the subproblem's objective as a constraint on
    # the value of the incumbent.
    qs = bd.cut_vars.values()
    expr.append([ list(ns.values()) + list(qs),
                 coefs + [-1] * len(qs) ])
    names.append("obj")
    rhs.append(0)
    senses.append("G")

    add_max_capacity(expr, names, rhs, senses, I, U, cap_u, b_i=b_i,
                     beta_r=beta_r, with_ms=True)
    add_bin_arrangement(expr, names, rhs, senses, I, U)

    bd.addConstrs(expr, senses, rhs, names)
