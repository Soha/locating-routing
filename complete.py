from benders.sub import SubPb
from load import add_balance, add_bin_arrangement, add_max_capacity
from load import load_master_constraints
from load import make_aux_vars, make_master_vars, make_sub_vars


class Complete(SubPb):
    def __init__(self):
        super(Complete, self).__init__()
        self._id = "mip"

    def make_vars(self, ns, xs, ms, vs, cins, cis, cin_u, d_ij, alfa):
        self.ns = ns
        self.xs = xs
        self.ms = ms
        self.vs = vs

        # Order needs to be the same as Benders
        names = list(self.ms.values()) + list(self.xs.values()) +\
            list(self.vs.values()) + list(self.ns.values())
        coefs = [0] * len(self.ms) + list(cis.values()) \
            + [0] * len(self.vs) + cins
        vtypes = "B" * len(self.ms) + "B" * len(self.xs) + \
            "C" * len(self.vs) + "B" * len(self.ns)
        lbs = [0] * len(vtypes)

        self.add_vars(obj=coefs, lb=lbs, names=names, types=vtypes)

    def make_cstrs(self, I, T, L, U, R, Q, TL, a_rt, s_i, d_ij, b_i, beta_r,
                   cap_u, use_vis):
        # Load constraints
        expr, names, rhs, senses = load_master_constraints(
            I, R, T, L, Q, TL, self.xs, self.ms, self.vs, a_rt, s_i, d_ij, b_i,
            beta_r, use_vis)
        add_max_capacity(expr, names, rhs, senses, I, U, cap_u, b_i, beta_r,
                         with_ms=True)
        add_bin_arrangement(expr, names, rhs, senses, I, U)

        self.add_cstrs(lin_expr=expr, senses=senses, rhs=rhs, names=names)

    def solve(self):
        self.cpx.solve()

    def results(self):
        raise Exception("Not supposed to use results in Complete")

    def value(self):
        return self.cpx.solution.get_objective_value()
