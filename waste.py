#
# Implement the Waste manageme models used in BranDec
#
import cplex

from collections import OrderedDict
from load import add_bin_arrangement, add_max_capacity, make_sub_vars

from benders.master import BendersException
from benders.sub import SubPb
from benders.utils import solValue, toKey
from benders.logger import Log


class WS(SubPb):
    """
    Waste sub-problem.
    """
    U_size = 0
    I_size = 0
    R_size = 0
    beta_r = []
    b_i = []
    master_names = []

    def __init__(self, I, U, R, cin_u, cap_u, beta_r_2, b_i_2, instance,
                 integer=False, verbose=False):
        super(WS, self).__init__()
        self._id = 0
        self.verbose = verbose

        status = self.cpx.solution.status
        self.optimal = [status.optimal, status.MIP_optimal]

        self.U_size = U
        self.I_size = I
        self.R_size = R  # Will be used to "extend" duals
        self.beta_r = beta_r_2
        self.b_i = b_i_2

        self.n_sol_values = [0] * I * U
        self.save_vars = integer  # Save solutions during post-processing
        self.solutions = OrderedDict()

        self.nnames, coef = make_sub_vars(I, U, cin_u)

        if integer:
            vtypes = "B"
        else:
            vtypes = "C"

        self.cpx.variables.add(obj=coef,
                               lb=[0] * (I - 1) * U,
                               names=list(self.nnames.values()),
                               types=[vtypes] * (I - 1) * U)

        # Load constraints
        names = []
        expr = []
        rhs = []
        senses = []

        add_max_capacity(expr, names, rhs, senses, I, U, cap_u)
        add_bin_arrangement(expr, names, rhs, senses, I, U)
        self.master_names = names[:(I - 1)]  # a)

        # Link to the previous error of not having subproblem variables for
        # depot i = 0. For this starting and stopping conditions of the for-loop
        # - for i in range(0, len(self.master_names)): - the garbage should be
        # of node i+1. This is, val = self.b_i[i+1] * self.beta_r[r] * duals[i].
        # If not the for-loop can be - for i in range(1, self.I_size): - but
        # then val = self.b_i[i] * self.beta_r[r] * duals[i -1]
        self.rhs = []
        for i in range(len(self.master_names)):
            for r in range(self.R_size):
                self.rhs.append(self.beta_r[r] * self.b_i[i + 1])

        # assert self.cpx.write_to_streamlinear_constraints.get_num() == (len(master_names) + len(non_master_names))
        # assert self.cpx.linear_constraints.get_names() == master_names + non_master_names

        if integer:
            self.add_cstrs(lin_expr=expr, senses=senses, rhs=rhs, names=names)

            if self.verbose:
                self.cpx.write("Solutions/" + instance + "/integer.lp")
        else:
            # Turn equality into two inequalities
            geq = names[(I - 1):]  # (1b)
            leq = ["le." + n for n in geq]
            # print(names[:(I - 1)], geq, leq)

            gexpr = expr[(I - 1):]
            lexpr = [(ind, [-1] * len(ind)) for ind, _ in gexpr]

            grhs = rhs[(I - 1):]
            lrhs = [-r for r in grhs]

            self.add_cstrs(lin_expr=expr[:(I - 1)],
                           senses=senses[:(I - 1)],
                           rhs=rhs[:(I - 1)],
                           names=self.master_names)
            self.add_cstrs(lin_expr=gexpr,
                           senses="G" * len(geq),
                           rhs=grhs,
                           names=geq)
            self.add_cstrs(lin_expr=lexpr,
                           senses="G" * len(leq),
                           rhs=lrhs,
                           names=leq)

            # Make upper bounds a constraint
            cnames = ["ub.{}".format(n) for n in self.nnames.values()]
            self.add_cstrs(lin_expr=[([n], [-1])
                                     for n in self.nnames.values()],
                           senses="G" * len(self.nnames.values()),
                           rhs=[-1] * len(self.nnames.values()),
                           names=cnames)

            self.cpx.set_problem_type(self.cpx.problem_type.LP)

            if self.verbose:
                self.cpx.write("Solutions/" + instance + "/continous.lp")

    def solve(self, solution):
        """
        Solve the problem using the master solution.
        """

        Log.debug("Use {} as solution.".format(solution))

        # Convert a solution of size $(I - 1) * R$ into a RHS vector of length
        # $I - 1$.
        rhs = []
        for i in range(len(self.master_names)):
            val = 0
            for r in range(self.R_size):
                loc = i * self.R_size + r
                val += solution[loc] * self.rhs[loc]

            rhs.append(val)

        Log.debug("Set RHS to {}".format(rhs))
        self.cpx.linear_constraints.set_rhs(zip(self.master_names, rhs))

        if self.verbose:
            self.cpx.write("integer_v2.lp")

        self.cpx.solve()

        # mu = self.cpx.solution.get_values()
        if self.save_vars:
            self.solutions[toKey(solution)] = dict(
                zip(self.cpx.variables.get_names(),
                    self.cpx.solution.get_values()))

        opt = self.cpx.solution.get_status()

        return opt in self.optimal

    def value(self):
        """
        Value of the optimal solution found as it is the primal.
        """
        return self.cpx.solution.get_objective_value()

    def optiCut(self):
        """
        Return the duals for an optimality cut.
        """
        duals = self.cpx.solution.get_dual_values()

        return duals[:len(self.master_names)], duals[len(self.master_names):]

    def feasCut(self):
        """
        Return the duals for a feasibility cut.
        """
        # Use a Farkas certificate as we have the primal and not the dual.
        ray, _ = self.cpx.solution.advanced.dual_farkas()

        return ray[:(len(self.master_names))], ray[(len(self.master_names)):]

    def results(self):
        """
        Return a formatted version of the dual coefficients.
        """
        code = self.cpx.solution.get_status()

        if code in self.optimal:
            # Log.debug("Optimality Cut with y = {}".format(
            Log.debug("Optimality Cut with n = {}".format(
                self.cpx.solution.get_values()))
            duals, rhs = self.optiCut()
        elif code == self.cpx.solution.status.infeasible:
            Log.debug("Feasibility Cut")
            duals, rhs = self.feasCut()
        else:
            raise BendersException(
                "Current sub solution is not valid: [{}] {}.".format(
                    code, self.cpx.solution.status[code]))

        lhs = [0] * len(self.master_names) * self.R_size
        for i in range(len(self.master_names)):
            for r in range(self.R_size):
                loc = i * self.R_size + r
                lhs[loc] = self.rhs[loc] * duals[i]

        mult = self.cpx.linear_constraints.get_rhs()[len(self.master_names):]

        rhs_f = solValue(rhs, mult)

        Log.debug("LHS: {}, RHS: {}".format(lhs, rhs_f))

        return lhs, rhs_f


class Round_WS(WS):
    """
    Upper bounding by rounding.
    """
    def __init__(self, I, U, R, cin_u, cap_u, beta_r, b_i, instance):
        self.cap_u = cap_u
        self.cin_u = cin_u
        super(Round_WS, self).__init__(I, U, R, cin_u, cap_u, beta_r, b_i,
                                       instance)

    def solve(self, solution):
        """
        Get the fractional solution.
        """
        super(Round_WS, self).solve(solution)
        self.solution = self.cpx.solution.get_values()

    def results(self):
        pass

    def value(self):
        """
        Find a valid bin arrangement for each GAP.
        """
        self.rounded_Obj_function = 0
        self.n_sol_values = OrderedDict(
            ((i, u), self.cpx.solution.get_values([self.nnames[i, u]])[0])
            for i in range(1, self.I_size) for u in range(self.U_size))

        for i in range(1, self.I_size):
            cap = 0
            count = []
            for u in range(self.U_size):
                if self.n_sol_values[i, u] > 0:
                    cap += self.n_sol_values[i, u] * self.cap_u[u]
                    count.append(u)

            if len(count) > 1:
                for u in range(self.U_size):
                    #bin arrangements are ordered according to increasing cost cin_u
                    if self.cap_u[u] >= cap:
                        self.n_sol_values[i, u] = 1
                        self.rounded_Obj_function += self.cin_u[u]

                        for u2 in range(self.U_size):
                            if u2 != u:
                                self.n_sol_values[i, u2] = 0
                        break
            else:
                self.rounded_Obj_function += self.cin_u[count[0]]

        # return sum(map(ceil, self.solution))
        return self.rounded_Obj_function
